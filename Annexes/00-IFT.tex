
\section{Iterative Feedback Tuning}

Data-driven approaches to achieving a control objective are often expressed as an optimisation problem. Generally, algebraically solving such a problem requires complete knowledge of the plant and the disturbances that are acting upon it. In practice this information is rarely available. 

Additionally, it is often the case the complexity of the controller is fixed. This is the case for example in many industrial applications were PID controllers are the norm. A model-order reduction step would have to be performed on the high order controller produced by the purely algebraic approach before the controller could be of practical use. This step discards all the optimality and stability guarantees that the algebraic approach confers.

An alternative approach is to rely on numerical methods to approximate a local optimum of the control objective. These typically rely on an iterative, gradient-based, minimisation procedure to improve the solution from an initial guess. The difficulty here is the calculation of the gradient of the control objective. 

The main innovation of IFT compared to previous methods is the realisation that this gradient can be estimated with some rather simple experiments on the plant. 

\begin{figure}[htbp]
    \centering
    \input{Figures/two_dof_block_diagram.tikz}
    \caption{The two degree of freedom control scheme}
    \label{fig:ift_two_dof_block_diagram}
\end{figure}

Consider the system shown in figure \ref{fig:ift_two_dof_block_diagram}. The true system is described by the unknown discrete time model

\begin{equation}
    y = G_0\cdot u + \nu   
\end{equation}

\noindent
where v is a discrete time unmeasurable disturbance which we will consider zero-mean. The system is controlled with a two degree of freedom controller such that  

\begin{equation}
     u = C_r(\rho) \cdot r - C_y(\rho) \cdot y
\end{equation}

\noindent 
where \( C_r \) and \( C_y \) are linear time-invariant transfer functions parametrised by some parameter vector \( \rho \in \mathbb{R}^{n_{\rho}} \) and \( r \) is an external deterministic reference signal independent of the disturbance \( \nu \). Any signal obtained whilst the controllers \( C_r(\rho) \) and \( C_y(\rho) \) are operating will be indicated with the \( \rho \)-argument. 

Let \( y^d \) be a desired output for the closed loop system when fed a reference signal \( r \). This signal can be described as the output of a reference model \( M_R \)

\begin{equation}
     y^d = M_R \cdot r
     \label{eq:ift_refmodel}
\end{equation} 

The error between the achieved response \( y(\rho) \) and the desired response \( y^d \) is 

\begin{equation}
    \begin{aligned}
        \tilde{y}(\rho) 
            &= y(\rho) - y^d \\ 
            &= \left( \frac{C_r(\rho) G_0}{1 + C_y(\rho) G_0} \cdot r + \frac{1}{1 + C_y(\rho) G_0} \cdot \nu \right) - y^d  
    \end{aligned}
\end{equation}

When a reference model has been defined as in equation \eqref{eq:ift_refmodel} the error can be re-written as:

\begin{equation}
    \begin{aligned}
        \tilde{y}(\rho) 
            &= \left( \frac{C_r(\rho) G_0}{1 + C_y(\rho) G_0} \cdot r + \frac{1}{1 + C_y(\rho) G_0} \nu \right) - M_r \cdot r \\
            &= \underbrace{\left( \frac{C_r(\rho) G_0}{1 + C_y(\rho) G_0} - M_R \right) \cdot r}_{\text{Tracking Error}} + \underbrace{\frac{1}{1 + C_y(\rho) G_0} \cdot \nu}_{\text{Disturbance}}
    \end{aligned}
\end{equation}

The first term, the tracking error of the actual closed loop system with respect to the desired behaviour is the quantity that will be optimised. The second term, the disturbance, cannot be directly minimised.

The achieved closed loop response and the sensitivity functions, respectively \( T_0(\rho) \) and \( S_0(\rho) \), are

\begin{equation}
    T_0(\rho) = \frac{C_r(\rho)G_0}{1 + C_y(\rho)G_0}, \qquad 
    S_0(\rho) = \frac{1}{1 + C_y(\rho)G_0}
\end{equation}

The meaning of the error may become clearer if we re-write it as

\begin{equation}
    \tilde{y}(\rho) = \left( T_0(\rho) - M_R \right) \cdot r + S_0(\rho) \cdot \nu
    \label{eq:ift_ytilde}
\end{equation}

A natural formulation of the control objective is the minimisation of the \( \mathcal{L}_2 \)-norm of the error between the desired and achieved responses. Consider the following criterion

\begin{equation}
    J(\rho) = \frac{1}{2N} \cdot E \left[ 
          \sum_{t=1}^N\left( L_y \tilde{y}(\rho) \right)^2  
        + \lambda \sum_{t=1}^N\left( L_u u(\rho) \right)^2
    \right]
\end{equation}

The optimal parameter vector \( \hat{\rho} \) is defined as

\begin{equation}
    \hat{\rho} = \argmin_{\rho} J(\rho)
\end{equation}

The first term of the criterion is the frequency weighted error between the desired and actual responses whilst the second term is a frequency weighted penalty on the control effort. In many cases these filters can be set to 1. 

Observe that when the reference model is \( M_R = 1 \) this problem becomes an LQG problem. 

After substituting the expression of \( \tilde{y}(\rho) \) found in equation \eqref{eq:ift_ytilde} into this cost function the criterion \( J(\rho) \) becomes

\begin{equation}
    \begin{aligned}
        J(\rho) 
            &= \frac{1}{2N} \cdot E \left[ \sum_{t=1}^N \left\{ L_y \left( T_0(\rho) - M_R \right)   \cdot r \right \}^2 \right] \\
            &+ \frac{1}{2N} \cdot E \left[ \sum_{t=1}^N \left\{ L_y s_0 \nu \right\}^2 \right] \\
            &+ \frac{1}{2N} \cdot \lambda \sum_{t=1}^N\left \{ L_u u(\rho) \right \}^2 
    \end{aligned}
\end{equation}

\noindent
Through simple mathematical passages and the observation that \( r \) is a noiseless signal we can further derive

\begin{equation}
    \begin{aligned}
        J(\rho) 
            &= \frac{1}{2N} \cdot \sum_{t=1}^N \left\{ L_y \left( T_0(\rho) - M_R \right)   \cdot r \right \}^2 \\
            &+ \frac{1}{2} \cdot E \left[ \left\{ L_y s_0 \nu \right\}^2 \right] \\
            &+ \frac{1}{2N} \cdot \lambda \sum_{t=1}^N\left \{ L_u u(\rho) \right \}^2 
    \end{aligned}
\end{equation}

In this formulation the first term is the tracking error, the second term is the effect of the disturbance and the third term is the control effort. In order to minimise this term we must be able to compute its gradient with regards to the controller parameters. 

In previous approaches to this problem the solution was to replace the true closed loop system with the reference model for the gradient computation. IFT in contrast uses input-output data collected from specific experiments to compute an approximation of the gradient at each iteration. 

The gradient of the criterion is: 

\begin{equation}
    \frac{\partial J}{\partial \rho}(\rho) = \frac{1}{N} \cdot E \left[ \sum_{t=1}^N \tilde{y}(\rho) \cdot \frac{\partial \tilde{y}}{\partial \rho}(\rho) + \lambda \sum_{t=1}^N u(\rho)\cdot \frac{\partial u}{\partial \rho}(\rho) \right]
\end{equation}

If the gradient could be computed, then the solution would be obtained with the following iterative algorithm

\begin{equation}
    \rho_{i+1} = \rho_i - \gamma_i R_i^{-1} \cdot \frac{\partial J}{\partial \rho}(\rho_i)
    \label{ift:iteration_step}
\end{equation}

\noindent
where \( R_i \) is an appropriate positive definite matrix and \( \gamma_i \) is positive scalar used to determine the step size. Since this problem is unsolvable algebraically an alternative approach must be found. 

We can however attempt to compute an unbiased approximation of \( \frac{\partial J}{\partial \rho}(\rho_i) \). This requires knowledge of the following quantities:

\begin{itemize}
    \item The signals \( \tilde{y}(\rho_i) \) and \( u(\rho_i) \)
    \item The gradients \( \pderiv{\tilde{y}}{\rho} \) and \( \pderiv{u}{\rho} \)
    \item The products \( \tilde{y}(\rho_i) \cdot \pderiv{\tilde{y}}{\rho} \) and \( u(\rho_i) \cdot \pderiv{u}{\rho} \)     
\end{itemize}

Only the last two quantities cause difficulties. We will now show how, through simple experiments on the plant, they can be computed. First observe that 

\begin{equation}
    \pderiv{\tilde{y}}{\rho} = \pderiv{y}{\rho}
\end{equation}

And 

\begin{equation}
    \begin{aligned}
        \pderiv{y}{\rho} 
            &= \pderiv{}{\rho} \left( \frac{C_r(\rho)G}{1 + C_y(\rho)G} \cdot r \right) - \pderiv{}{\rho} \left( \frac{1}{1 + C_y(\rho)G} \cdot \nu \right) \\
            &= \frac{\pderiv{C_r}{\rho}G \left( 1 + C_y G \right) - C_r G \pderiv{C_y}{\rho}G}{ \left( 1 + C_y G \right)^2 } \cdot r + \frac{G}{ \left( 1  + C_y G \right)^2 }\pderiv{C_y}{\rho} \cdot \nu \\
            &= \frac{G}{ \left( 1 + C_y G \right)^2 } \cdot \pderiv{G_R}{\rho} \cdot r 
             - \frac{C_r G^2}{ \left( 1 + C_y G \right)^2 } \cdot \pderiv{C_y}{\rho} \cdot r 
             - \frac{G}{ \left( 1 + C_y G \right)^2 } \cdot \pderiv{C_y}{\rho} \cdot \nu \\
            &= \frac{T_0(\rho)}{C_r} \cdot \pderiv{G_R}{\rho} \cdot r
             - \frac{T_0(\rho)^2}{C_r} \cdot \pderiv{C_y}{\rho} \cdot r
             - \frac{T_0(\rho) S_0(\rho)}{C_r} \cdot \pderiv{C_y}{\rho} \cdot \nu \\
            &= \frac{T_0(\rho)}{C_r} \cdot \pderiv{C_y}{\rho} \cdot r 
             - \frac{1}{C_r} \left( T_0(\rho)^2 \cdot r + T_0(\rho) S_0(\rho) \cdot \nu \right) \pderiv{C_y}{\rho}
    \end{aligned}
\end{equation}

Notice that the last term can be re-written as:

\begin{equation}
    T_0(\rho)^2 \cdot r + T_0(\rho) S_0(\rho) \cdot \nu = T_0 \cdot y
\end{equation}

Which allows us to rewrite the equation as

\begin{equation}
    \begin{aligned}
        \pderiv{y}{\rho} 
            &= \frac{1}{C_r} \left( T_0(\rho) \cdot \pderiv{C_y}{\rho} \cdot r  - T_0(\rho) \pderiv{C_y}{\rho} \cdot y \right) \\
            &= \frac{1}{C_r} \left( T_0(\rho) \cdot \pderiv{C_y}{\rho} \cdot r  - T_0(\rho) \pderiv{C_y}{\rho} \cdot y + T_0(\rho) \pderiv{C_y}{\rho} \cdot y - T_0(\rho) \pderiv{C_y}{\rho} \cdot y \right) \\
            &= \frac{1}{C_r} \left[\left( \pderiv{C_r}{\rho} - \pderiv{C_y}{\rho} \right) \cdot T_0(\rho) r + \pderiv{C_y}{\rho}T_0(\rho) \left( r - y \right) \right]
    \end{aligned}
\end{equation}


Do tests ...

show equivalence 

Do the same with the input  signals 
