
\chapter{Tuning Experiment}
\label{chap:tuning_procedure}

The most important pre-requisite for data-driven algorithms such as VRFT is the collection of a sufficiently long persistently exciting input-output dataset. 

Since the aim of this work is to tune the controllers on the pitch regulation loop all the tests were performed on a test bed that fixes all the degrees of freedom of the quad copter except the its pitch axis. The test bed is described in detail in section \ref{sec:quad_test_bed}. 

The control scheme for the pitch control loop is shown in Figure \ref{fig:tuning_control_scheme}. See section \ref{sec:quad_pitch_ctrl} for a detailed description of each component and the symbols used. 

\begin{figure}[htbp]
    \centering
    \input{Figures/quad_control_scheme.tikz}
    \caption[Control Scheme For The Pitch Control Loop]{Control Scheme For The Pitch Control Loop where \( q \) is the pitch rate of the quadcopter, \( \Theta \) is its pitch angle, \( \delta \Omega_P \) is the change in speed of the propellers and \( \delta M \) is the change in torque requested by the inner controller.}
    \label{fig:tuning_control_scheme}
\end{figure}

The VRFT procedure considers the input-output data gathered on the plant. To follow the letter of the VRFT approach as described in Chapter \ref{chap:vrft} we should consider the \emph{inner plant} as the combination of the mixer \( \chi \) and the transfer function \( G \). It follows that the integrator \( \frac{1}{s} \) should be considered as the \emph{outer plant}.

The experiments should be performed in open loop, providing \( \delta M \) as an input and reading the signals \( q \) and \( \Theta \) as our \emph{inner} and \emph{outer} outputs. Unfortunately the firmware does not allow us to provide \( \delta M \) as an input or read the pitch angle \( \Theta \) directly when operating in open loop conditions. Instead we specified as the input \( \delta \Omega_P \) the change in the speed of the propellers and read \( q \) as the single output. 

This is not an issue however since the mixer, considering only the pitch control loop, is a known scalar. This enabled us to calculate the plant input as 

\begin{equation}
    \delta M = \chi^{-1} \delta \Omega_P.
\end{equation}

The \emph{outer} output signal can also be calculated algebraically. Whilst in the general case the outer plant model is unknown, in our case the outer plant is simply an integrator. The pitch angle \( \Theta \) is simply the integral of the pitch rate \( q \) over time. Consequently we were able to calculate the pitch angle as 

\begin{equation}
    \Theta = \Integrate{q}{t,0,t} 
    \label{eq:tuning_calc_theta}
\end{equation}

With the quadcopter firmware operating in open loop a \textbf{P}seudo \textbf{R}andom \textbf{B}inary \textbf{S}equence (PRBS) was generated for the input \( \delta \Omega_P \) and fed into the system with a time step of \num{0.1}\si{\second}. This sequence was generated in such way as to reliably excite the dominant dynamics of the system. The input signal and the computed \( \delta M \) are show in Figure \ref{fig:tuning_prbs_inputs}.


\begin{figure}[htbp] 
    \centering
    \input{Figures/Plots/prbs_inputs.tikz}
    \caption[Tuning Experiment Inputs]{Measured \( \delta \Omega_P \) and computed control variable \( \delta M \). Measured signals are represented with a continuous line, computed signals are shown with a dotted line}
    \label{fig:tuning_prbs_inputs}
\end{figure}

The output measurements were acquired and logged by the quadcopter itself using its on-board IMU with a time step of \num{0.1}\si{\second}. The pitch angle \( \Theta \) was computed from the pitch rate measurements as shown in equation \eqref{eq:tuning_calc_theta}. The outputs of the this identification procedure are shown in Figure \ref{fig:tuning_prbs_outputs}.

\begin{figure}[htbp] 
    \centering
    \input{Figures/Plots/prbs_outputs.tikz}
    \caption[Tuning Experiment Outputs] {Measured pitch rate (\( q \)) and computed pitch angle (\( \Theta \)). Measured signals are represented with a continuous line, computed signals are shown with a dotted line}
    \label{fig:tuning_prbs_outputs}
\end{figure}
