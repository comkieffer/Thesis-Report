
\chapter{Introduction}
\label{chap:quadrotor_dynamics}


\section{Component Specifications}
\label{sec:quad_cs}

The quadcopter used for this work was entirely developed at Politecnico di Milano based on the following requirements:

\begin{itemize}
    \item \emph{X} frame of medium size (\num{450} to \num{550} \si{\milli\metre} distance between opposite rotors)
    \item Overall weight of less than \kg{2} 
    \item Payload of at least \kg{0.5}
    \item Flight time of at least 10 minutes
\end{itemize}

The specifications of the components chosen in order to achieve these goals are documented in the following sections. A fixed-pitch actuation scheme was retained since the performance requirements of the system are relatively low. For a more detailed explanation behind these choices see \cite{Giurato2015DesignFixed}

\begin{figure}[htbp]
    \centering

    \begin{subfigure}[t]{0.45\textwidth}
        \centering
        \includegraphics[width=.95\textwidth]{hobby_king_talon_20}
        \caption{Hobby King Talon V2.0 Frame}
        \label{subfig:quad_frame}
    \end{subfigure}
    \begin{subfigure}[t]{0.45\textwidth}
        \centering
        \includegraphics[width=.95\textwidth]{rctimer_hp2814}
        \caption{RCTimer HP2814 Motor}
        \label{subfig:quad_motor}
    \end{subfigure}
    \begin{subfigure}[t]{0.45\textwidth}
        \centering
        \includegraphics[width=.95\textwidth]{rctimer_nfs_esc_30a}
        \caption{RCTimer NFS ESC 30A}
        \label{subfig:quad_esc}
    \end{subfigure}
    \begin{subfigure}[t]{0.45\textwidth}
        \centering
        \includegraphics[width=.95\textwidth]{12_inch_prop}
        \caption{12 inch, \num{4.5} \si{\degree} pitch propellers}
        \label{subfig:quad_prop}
    \end{subfigure}
    \begin{subfigure}[t]{0.45\textwidth}
        \centering
        \includegraphics[width=.95\textwidth]{support}
        \caption{Electronic boards support}
        \label{subfig:quad_support}
    \end{subfigure}
    \begin{subfigure}[t]{0.45\textwidth}
        \centering
        \includegraphics[width=.95\textwidth]{vibr_dampers}
        \caption{Vibration dampers}
        \label{subfig:quad_vibr_damper}
    \end{subfigure}
    
    \caption{Quadrotor Components}
    \label{fig:quad_hk_plat}
\end{figure}

\subsection{Frame}

The frame (Figure \ref{subfig:quad_frame}) is the central building block of the quadcopter. The Talon V2.0 frame, built out of carbon fibre and aluminium was retained due to its low weight and high strength. The distance between opposite motors on this frame is \num{500} \si{\milli\metre}.

\subsection{Motors, ESC \& Propellers}

The motors (Figure \ref{subfig:quad_motor}) are brushless DC motors (BLDC) from the RCTimer High Performance series (HP2814). Since brushless motors are synchronous they cannot be driven by a simple DC current but require an inverter to provide a switching electric signal. This is the \textbf{E}lectronic \textbf{S}peed \textbf{C}ontroller (ESC). In this case, the RCTimer NFS ESC 30A (Figure \ref{subfig:quad_esc}). This combination of motor and driver is able to push the propeller to close to 8000\ \rpm

The propellers mounted on the motors have two \num{12} \si{inch} blades with a \num{4.5}\si{\degree} pitch angle (Figure \ref{subfig:quad_prop}).

\subsection{Battery}

The batteries used on the quadrotor are Turnigy nano-tech \num{4000} \si{\milli\ampere\hour}. These are LiPo 3 cell batteries with a nominal tension of \num{3.7} \si{\volt} per cell for a total tension of \num{11.1} \si{\volt} per battery. The batteries are capable of supplying a constant current of \num{100} \si{\ampere} with peaks of up to \num{200} \si{\ampere} for short periods of time.

\subsection{Vibration Damping}

To reduce the mechanical vibrations transmitted through the frame from the motors a support plate onto which the electronics are mounted was realised (Figure \ref{subfig:quad_support}). This plate is then fixed to the frame with vibration damping rubber feet (Figure \ref{subfig:quad_vibr_damper}).

\subsection{Flight Control Unit \& Firmware}

The flight control unit is the brains of the quadrotor. It was implemented on a \textbf{R}apid \textbf{R}obot \textbf{P}rototyping (R2P) boards~\cite{Bonarini2014R2P}. This is an open source hardware and software framework that enables the rapid development of robotic applications. These boards implement an IMU, serial communication, control of the RC motors with PWM signals and the actual flight control. The modules use a publish/subscribe architecture to communicate.

The control portion is implemented in Simulink and compiled to C++ code. The firmware manages the communication between the sensors and the generated code in order to control the quadcopter. 

\subsection{Test Bed}
\label{sec:quad_test_bed}

All the tests on the quadrotor were performed on a test bed that constrains all translational degrees of freedom as well as the roll and yaw motions. Only the pitch rotation is left unconstrained. This ensures that the tests are repeatable and safe and that an erroneous choice of the controller parameters will not send the system crashing through the room. 

\begin{figure}[htbp]
    \centering

    \begin{subfigure}[t]{0.45\textwidth}
        \centering
        \includegraphics[width=.95\textwidth]{test_bed_1}
        \caption{Test Bed, Front View}
        \label{subfig:quad_test bed_1}
    \end{subfigure}
    \begin{subfigure}[t]{0.45\textwidth}
        \centering
        \includegraphics[width=.95\textwidth]{test_bed_2}
        \caption{Test Bed, Top view}
        \label{subfig:quad_test_bed_2}
    \end{subfigure}
\end{figure}

The test bed is built out of \emph{x-frame} aluminium rods and weighted with sacks of concrete. The upper part of the frame has a smooth rod resting on ball bearings at each extremity for frictionless rotation. The quadrotor is then securely fastened to this rod. 

In the current mounting scheme the rod passes as close as possible to the centre of mass of the system in order to interfere as little as possible with the dynamics of the quadrotor. Because of the physical configuration of the system there is small distance between the rod and the actual centre of mass. In turn, this causes the system to act like a very small pendulum and the test bed adds some damping when the system quadrotor achieves higher pitch angles. In practice this damping is negligible for small oscillations.

The test bed holds the quadcopter high enough that ground effect disturbances are avoided however, since the test takes place in a closed space some recirculation of rotor wakes occurs. This represents a discrepancy when compared to outdoor flight conditions where the rotor wakes develop free from obstacles. Even so, it has been shown in previous work that such a test bed is representative of actual attitude dynamics in flight.

\subsection{Additional Flight Hardware}

The quadcopter also holds a RaspberryPi 2 board used to interface the R2P modules with a ROS network but this functionality was unused for the tests. In addition a small ultrasonic sensor has been mounted on the drone to measure the distance from the ground when landing. 
