
\chapter*{Introduction} 
\addcontentsline{toc}{chapter}{Introduction}
\label{chap:introdction}

Throughout history the development of heavier than air vehicles can generally be split between fixed wing aircraft (\ie aeroplanes) and rotorcraft (\ie helicopters). The former use engines aligned along the longitudinal axis of the vehicle to provide forward thrust, fixed wings for lift  and a system of control surfaces to generate the control torques and forces. The latter rely on a single large main rotor where the pitch angle of each blade can be controlled which provides both lift and thrust. A smaller rotor is used to provide a counter-torque counteracting the torque of the main rotor. The vehicle is then controlled by varying the individual pitch of the rotor blades over the course of each rotation. With few exceptions, such as tilt-wing aircraft that have elements of both fixed and rotary wing aircraft, this separation has remained accurate until recently. 

Multirotor vehicles have emerged as a radically different approach to heavier than air flight. These are rotary wing aircraft that, instead of depending on a large main rotor for lift and control, employ many smaller rotors. Whereas a single large rotor requires a powerful (and thus expensive) motor to power, smaller rotors can be powered with smaller motors whose cumulative cost is less than the single large motor. 

The downside is that the number of motors significantly complicates the control scheme. Simple rotations and translations now require the combined use of several motors which, in turn, requires a complex on-board flight control unit (FCU). This FCU usually implements the different control loops using data from an on-board inertial measurement unit (IMU) that provides a continuously updated view of the aircraft's orientation.

In turn the reduction in costs has created new opportunities: multi rotor drones are used for low-cost aerial surveying and mapping, airborne video and photography or, more recently, delivery. This leads to a wide variety of payloads with different sizes and weights. This wide range of applications has also lead to a wide variety of vehicles: multirotors with three, four, six or even eight rotors are quite common. Of these, the most common configuration is without a doubt the quadrotor: a machine with with four rotors. These have been widely used to fly small payloads in the neighbourhood of \num{1}\si{\kilo\gram}, are relatively low cost and simple to control. 


\section*{Thesis Description}

The problem of attitude control in quadrotors is the focus of this thesis. With such a wide variety of vehicles sizes and applications a \emph{one-size-fits-all} approach cannot provide satisfactory results. The individual attitude controllers must be tuned manually to achieve acceptable levels of performance. This is usually done by first producing an accurate physical model of the quadcopter before applying classical control theory methods such as \hinf tuning to synthesise a suitable controller. This procedure is long, complex but most of all, is only performed once over the lifetime of the vehicle and cannot account for the degradation of the components as they age.

Data-driven methods represent an alternate approach to the controller synthesis problem. Instead of requiring and accurate model of the system they rely entirely on the availability of a set of input-output data measured in open loop conditions and directly produce the parameters of a controller minimising a specified cost criterion. 

These approaches are attractive firstly because they do not require an accurate model of the system to be controlled but most of all because they can be easily re-run to account for changes in the hardware of the system. One could for example periodically re-tune the controllers to account for the ageing of the components or even temperature variations. It may also be possible to leverage this approach to automatically re-tune the controllers in flight to account for the specific size and shape of the payload.

This thesis represents a single early step on the way to such auto-tuning systems. In it the applicability of a specific data-driven controller synthesis technique: Virtual Reference Feedback Tuning to the pitch control loop and the performance levels that can be achieved are explored. The results were compared with a pre-existing \hinf-tuned controller.

\section*{Thesis Structure }

The first chapter of this thesis aims to provide the reader with a more in-depth understanding of multirotor systems in general and, considering quadrotors specifically, the design considerations behind their structure. The second half of the chapter concerns itself with an explanation of how a quadrotor moves and turns, which control inputs are required. This leads us to consider the individual pitch, roll and yaw control loops and their structure. 

The second chapter looks at controller tuning, comparing the classical approach with model-reference based methods. In talking about the latter model-based and data-driven approaches were considered before several different data-driven controller synthesis methods were detailed.  After explaining why VRFT is the better choice for this problem an in depth explanation of this method is provided. 

The third chapter looks at how the requirements on the system were translated for the VRFT method and how a known model can be used to guide the VRFT procedure. It shows how the simulated model was built from the pre-existing knowledge of the machine and how this knowledge was used to inform the structure of the reference models used to specify the performance for the VRFT. Finally it shows how a set of input-output data was simulated to be used for the VRFT and the performance of the simulated model.

The fourth and final chapter presents the specifics of the actual quadrotor that was used for which the controller was tuned. It presents a brief overview of the hardware components and the test-bed. It then shows how the input-output data required for VRFT was collected and processed, how the reference models used were identified and finally it presents the results of the experiments testing campaign.
