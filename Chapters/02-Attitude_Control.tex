
\chapter{Quadrotors \& Attitude Control}
\label{chap:attctrl}

Quadrotors are multi-rotor aerial vehicles that exploit differential thrust from their motors to move. Whilst quadrotors are the most common form of multi-rotor in use today they are not by any means the only one: configurations with three (tricopters), six (hexacopters) and eight rotors (octocopters) have also seen significant use.

Most multi-rotor designs strive to use relatively inexpensive commodity motors to keep the system affordable. To that end, when trying to increase the payload of the system it is often preferable to increase the number of motors instead of sourcing more powerful and more expensive motors capable of driving larger propellers. This naturally increases the effective payload of the system as well as its redundancy. Whereas the loss of one motor on a tricopter or a quadcopter would have disastrous consequences on the controllability of the system, the loss of one motor on an octocopter might be relatively unnoticeable. The downside is that increasing the number of motors leads to a lower global efficiency compared to simply increasing the swept area of the rotors.

Quadcopters today have emerged as the standard architecture for lightweight systems with payloads weighing in the neighbourhood of \kg{1}. They provide acceptable levels of cost, redundancy, payload and performance and are used widely by hobbyists and professionals alike.    

In Section \ref{sec:quad_theory} the general design considerations that must be taken into account when designing a quadcopter will be briefly discussed following which the control inputs necessary to achieve the various motions of the system will  be shown in Section \ref{sec:quad_control}. Finally, in Section \ref{sec:quad_att_ctrl} the various attitude control loops will be shown.

\section{Design Considerations}
\label{sec:quad_theory}

Unlike fixed wing aircraft that employ a complex system of control surfaces to generate control forces and moments, multicopters can only vary the thrust provided by each propeller. This can be achieved in a number of ways each providing different trade-offs. 

The most common way to vary the thrust of each propeller is to vary the speed of each motor. This is the simplest way from both a mechanical and control theory standpoint. By increasing or decreasing its speed, the amount of thrust produced by each propeller can be increased or decreased. This simplicity comes at a cost: the time constants of an electric motor spinning at several thousand \rpm\ are comparatively high which limits the overall agility of the  system. 

A scheme that allows for faster reactions at the cost of complexity is to tilt the arms onto which the motors are attached. In this configuration small changes in vertical thrust can be achieved with relatively small rotations of the arms. Whilst these can be actuated rather quickly, it is not possible to obtain large changes to the vertical thrust without also varying the speed of the propellers. 

The fastest but most complex way of varying thrust is to control each propeller like the main rotor of a helicopter. A mechanical linkage allows the control system to change the pitch angle of the blades of the propeller. This allows extremely fast changes in thrust but is significantly more complex mechanically. This configuration is generally reserved for larger quadcopters where the inertia of the propellers might make the previous methods too slow.

A second important design choice is whether to adopt an \emph{X} or a \emph{+} structure. In the first configuration the motors are located on each side of the system, one at each tip of the \emph{X}. In the latter configuration two motors are situated along the longitudinal axis of the system: one in front of and one behind the centre. The other two motors are placed along the lateral axis of the system: one right and one left of the centre.

The \emph{X} configuration is often retained because it can provide more rotational acceleration and, if a camera is attached to the system it can be pointed forward without the landing gear being in its field of view. 

The quad copter used for this work has an \emph{X} configuration and fixed pitch rotors. 

\section{Control of a Fixed Pitch Quadcopter}
\label{sec:quad_control}

In a fixed pitch quadrotor the only way to vary the attitude of the system is to intervene on the speed of the motors. Thus, with only four actuators to control the six spatial degrees of freedom the system is underactuated. Vertical movement as well as the pitch, roll and yaw dynamics is directly controlled. Forward and lateral translation movements however must be controlled with the dynamics of the whole system.

\begin{figure}[htbp]
    \centering
    \input{Figures/rpy.tikz}
    \caption{Illustartion of the Roll, Pitch \& Yaw Motions}
    \label{fig:quad_rpy}
\end{figure}

The pitch, roll, yaw and elevation motions of the quadcopter are uncoupled. Changing the yaw angle does not affect the roll or pitch dynamics for example. From a practical standpoint this means that the problem of designing a controller can be reduced from a MIMO problem to a set of independent SISO problems. The outputs of the different controllers are simply summed to obtain a combined motion.

Additionally, unlike helicopters, quadrotors do not require anti-torque tail rotors. This is achieved by having half of the propellers rotate in a clockwise direction and half rotate in a counterclockwise direction. As shown in Figure \ref{fig:quad_rot_dir}, on the vehicle used for this work, rotors 1 (front left) and 3 (rear right) rotate clockwise whilst rotors 2 (front right) and 4 (rear left) rotate counter clockwise.

\begin{figure}[htbp]
    \centering
    \input{Figures/quad_rotor_dir.tikz}
    \caption{Rotation directions of the quadrotor propellers}
    \label{fig:quad_rot_dir}
\end{figure}

\subsection{Elevation}

To maintain the quadcopter in stable hovering flight a base speed (\( \Omega_H \)) corresponding to a nominal \si{\rpm} must be applied to all the propellers to provide a thrust sufficient to counter gravitational effects. To fly upwards or downwards it is sufficient to vary the speed of all four rotors by the same amount \( \delta \Omega_T \) such that

\begin{equation}
    \Omega_{1,2,3,4} = \Omega_H + \delta \Omega_T
\end{equation}

\noindent
where \( \Omega_1 \), \( \Omega_2 \), \( \Omega_3 \) and \( \Omega_4 \) are respectively the speeds of rotors \num{1}, \num{2}, \num{3} and \num{4}. This generates a vertical force along the \( z_B \) axis.

\subsection{Roll}

Roll motion (a rotation around the longitudinal axis) is achieved by increasing the speed of the rotors on the left of quadcopter (1, 4) by some amount \( \delta \Omega_R \) and decreasing the speed of the rotors on the right (2, 3) by the same amount. This generates a torque around the \( x_B \) axis of the quadcopter.

\begin{equation}
     \begin{aligned}
        \Omega_{1,4} &= \Omega_H + \delta \Omega_R \\
        \Omega_{2,3} &= \Omega_H - \delta \Omega_R.
     \end{aligned}
\end{equation} 

\subsection{Pitch}

Pitch motion (a rotation around the lateral axis) is obtained by increasing the speed of the rotors on the front of the quadcopter (1, 2) by some amount \( \delta \Omega_P \) and decreasing the speed of the rotors on the rear (3, 4) by the same amount. This generates a torque around the \( y_B \) axis of the quadcopter.

\begin{equation}
     \begin{aligned}
        \Omega_{1,2} &= \Omega_H + \delta \Omega_P \\
        \Omega_{3,4} &= \Omega_H - \delta \Omega_P.
     \end{aligned}
\end{equation} 

\subsection{Yaw}

Yaw motion (a rotation around the vertical axis) is achieved by increasing the speed of the clockwise turning rotors by some amount \( \delta \Omega_Y \) and decreasing the speed of the counterclockwise turning rotors by the same amount. This generates an imbalance of angular momenta around the \( z_B \) axis which causes the system to rotate around the vertical (\( z_B \)) axis . 

\begin{equation}
     \begin{aligned}
        \Omega_{1,3} &= \Omega_H + \delta \Omega_Y \\
        \Omega_{2,4} &= \Omega_H - \delta \Omega_Y.
     \end{aligned}
\end{equation} 

\subsection{Longitudinal \& Lateral translation}

Longitudinal and lateral motions cannot be controlled directly with just four control variables. Instead the dynamics of the entire system are used to obtain these motions. Specifically, to move the quadcopter forward the system is tilted forward (pitched forward). To move backward the opposite motion is applied. The same reasoning applies to lateral translations where instead of tilting around the pitch axis the system is rolled left and right.


\section{The Attitude Control Loops}
\label{sec:quad_att_ctrl}

A closed loop control system is required in order to precisely control the orientation of the aircraft. In Section \ref{sec:quad_control} we showed that the rotational degrees of freedom of the vehicle are uncoupled and that the attitude control problem can be viewed as a set of independent SISO problems.

When looking at a \emph{X}-frame quadcopter, after removing any bodywork and aerodynamic fairings, it can be quite difficult to differentiate the front and the sides of the system. This symmetry  is exploited when designing the control system: the pitch and roll control loops can generally be considered identical. Only the yaw regulation loop differs. 

Rather counter-intuitively the pitch and roll loops are stable. They must however be extremely reactive in order to compensate for external disturbances such as wind gusts.Thus, to maintain the vehicle in stable flight it is essential that these control loops be as fast as possible. The performance of the yaw control loop by contrast is imposed not by physical imperatives but by the ability of the pilot. Whilst it would be possible to achieve extremely high turn rates a human pilot must be able to control the vehicle and as such the bandwidth of this control loop is kept artificially low. This difference is apparent when comparing the structure of the pitch, roll and yaw control loops. On the quadcopter used for this work the pitch and roll control are implemented with a nested controller scheme whilst the yaw control loop uses a simpler single-loop architecture. 


\subsection{The Pitch Control Loop} 
\label{sec:quad_pitch_ctrl}

The pitch control loop employs two nested loops to achieve the best possible performance. This is due to the fact that the pitch rate dynamics of the system, controlled by modifying the speed of the rotors, are considerably faster than the pitch angle dynamics.

\begin{figure}[htbp]
    \centering
    \input{Figures/pitch_ctrl_loop.tikz}
    \caption{The pitch control loop}
    \label{fig:quad_pitch_loop}
\end{figure}

The controllers implemented in the firmware of our quadcopter have a fixed structure. The outer loop controller is a PD whereas the inner loop controller is a PID. These controllers are expressed in the ideal parallel form

\begin{equation}
    \begin{aligned}
         PD(s) &= K_{p_i} + \frac{K_{d_i}s}{T_fs + 1}  \\
        PID(s) &= K_{p_o} + \frac{K_{i_i}}{s} + \frac{K_{d_i}s}{T_fs + 1}
    \end{aligned}    
\end{equation}

\noindent
where \( K_{p_i} \), \( K_{i_i} \) and \( K_{d_i} \) are respectively the proportional, integral and derivative gains of the the PID controller, \( K_{p_o} \) and \( K_{d_o} \) are respectively the proportional and derivative gains of the the PD controller and \( T_f \) is the filter time constant.

The control variable produced by the inner controller is the torque around the pitch axis. The real input to the plant however is the difference in speed between the front rotors (numbers 1 \& 2) and the rear rotors (numbers 3 \& 4). The transformation from a torque to a speed difference is the responsibility of the mixer matrix \( \chi \). 

Consider the fact that each propeller generates a vertical thrust and, due to the distance between the centre of the propeller and the centre of mass of the vehicle, a torque. Consequently each propellers contributes to the vertical  thrust \( T \) and the moments \( L \), \( M \) and \( N \) around, respectively, the pitch, roll and yaw axes. It can be shown that the cumulative force generated by the propellers is 

\begin{equation}
    F_{Props} = \begin{bmatrix}
        0 \\ 0 \\ K_T \left( \Omega_1^2 + \Omega_2^2 + \Omega_3^2 + \Omega_4^2 \right)
    \end{bmatrix}
\end{equation}

\noindent
where \( \Omega_i \) is the rotational speed of the \( i \)-th motor and \( K_T \) is a known constant. It can also be shown that cumulative moments \( L \), \( M \) and \( N \) around the \( x_B \), \( y_B \) and \( z_B \) axes are

\begin{equation}
    M_{props} = \begin{bmatrix}
        L \\ M \\ N
    \end{bmatrix} = \begin{bmatrix}
        K_T \frac{b}{\sqrt{2}} \left( \Omega_1^2 - \Omega_2^2 - \Omega_3^2 + \Omega_4^2 \right) \\
        K_T \frac{b}{\sqrt{2}} \left( \Omega_1^2 + \Omega_2^2 - \Omega_3^2 - \Omega_4^2 \right) \\
        K_Q \left( -\Omega_1^2 + \Omega_2^2 - \Omega_3^2 + \Omega_4^2 \right) \\
    \end{bmatrix}     
\end{equation}   

\noindent
where \( K_Q \) is another known constant. The forces and moments can be rearranged in order to isolate on one side \( L \), \( M \), \( N \) and \( T \) and on the other \( \Omega_1^2 \), \( \Omega_2^2 \), \( \Omega_3^2 \) and \( \Omega_4^2 \)

\begin{equation}
    \begin{aligned}
    \begin{bmatrix}
        T \\ L \\ M \\ N
    \end{bmatrix} &= \begin{bmatrix}
        K_T                    & K_T                     & K_T                     & K_T \\
        K_T \frac{b}{\sqrt{2}} & -K_T \frac{b}{\sqrt{2}} & -K_T \frac{b}{\sqrt{2}} &  K_T \frac{b}{\sqrt{2}} \\
        K_T \frac{b}{\sqrt{2}} &  K_T \frac{b}{\sqrt{2}} & -K_T \frac{b}{\sqrt{2}} & -K_T \frac{b}{\sqrt{2}} \\
        K_Q                    & K_Q                     & K_Q                     & K_Q
    \end{bmatrix} \begin{bmatrix}
        \Omega_1^2 \\ \Omega_2^2 \\ \Omega_3^2 \\ \Omega_4^2
    \end{bmatrix} \\
     &= \chi \begin{bmatrix}
        \Omega_1^2 \\ \Omega_2^2 \\ \Omega_3^2 \\ \Omega_4^2
    \end{bmatrix}
    \end{aligned}
\end{equation}

\noindent
where \( \chi \) is the mixer matrix which relates the required thrusts and moments to around each axis to the rotational speed of each propeller.  


\subsection{The Roll Control Loop}

The roll control loop as implemented on the quadcopter that was used for this work is identical to the pitch control described in Section \ref{sec:quad_pitch_ctrl}. It differs only in the pairs of rotors it considers. The input to the plant is the difference in speed between the right rotors (numbers 1 \& 4) and the left rotors (number 2 \& 3).

\subsection{The Yaw Control Loop}

Due to its lower bandwidth requirements the Yaw control loop is the simplest of the attitude control loops. 

\begin{figure}[htbp]
    \centering
    \input{Figures/yaw_ctrl_loop.tikz}
    \caption{The yaw control loop}
    \label{fig:quad_yaw_loop}
\end{figure}

On this quadcopter the controller is implemented as a simple PI expressed in the ideal parallel form

\begin{equation}
    PI(s) = K_p + K_i \frac{1}{s}
\end{equation}

\noindent 
which controls the angular rate of the system around the yaw axis. Unlike the pitch and roll loops the yaw control loop groups the motors by the direction of rotation of the propellers. The real control input is the difference in speed between the clockwise turning and the counterclockwise turning propellers. The transformation from an angular momentum to a speed difference is assured by the mixer matrix.  

