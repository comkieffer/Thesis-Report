
\chapter{Simulation Results}
\label{chap:simres}

One of the main selling points of data-driven controller tuning methods is that they do not require a detailed mathematical description of the plant. However, if is a model is available, it can be used as a guide to evaluate the performance of the tuned controllers without having to run extensive tests on the test-bed. 

In Section \ref{sec:simres_syst} a model of the quadcopter will be introduced. In Section \ref{sec:simres_mref_struc} how this model was used to refine the structure of the reference models will be shown. In Section \ref{sec:simres_ctrl_fam} the controller families for the inner and outer loops used for VRFT will be shown and, finally, in Section \ref{sec:simres_simres} some controllers generated with VRFT and the performance indicated by the simulations will be shown.

\section{Simulated Model}
\label{sec:simres_syst}

\subsection{Pitch Dynamics}

The quadcopter used in this work has been extensively characterised and modelled in \cite{Giurato2015DesignFixed}. For the purpose of simulation the pitch dynamics of the system can be reduced to two blocks:  a simple model of the pitch dynamics \( G_q(s) \) and an integrator used to compute the current pitch angle. This structure is shown in Figure \ref{fig:simres_simple_bd}. 

\begin{figure}[htbp]
    \centering
    \input{Figures/simplified_quad_model.tikz}
    \caption{Simplified model of the quadcopter}
    \label{fig:simres_simple_bd}
\end{figure}

The model of the pitch dynamics obtained in the cited work

\begin{equation}
    G_q(s) = \frac{0.423}{s + 1.33}
\end{equation}

\noindent
is a linearisation, in the origin, of the transfer function from \( \delta \Omega \), the requested change in the speed of the propellers, to \( q \), the pitch rate. 

This transfer function does not attempt to account for any non linear behaviours of the system such as saturations. It does not consider the dynamics of the motors or any aerodynamic drag effects. In practice this is not an issue as long the dynamics of the simulated system are slow enough not to interfere with the motor dynamics.

The calculation of the pitch angle as the integral of the pitch rate represents a divergence from the real system. In the real system the pitch angle is not directly available but is estimated as part of a sophisticated sensor fusion algorithm by the IMU the dynamics of which are not accounted for in this approximation. The signal \( \vartheta \) as computed in Figure \ref{fig:simres_simple_bd} is the real pitch angle. 

This approximation is used for the simulations as it makes the two nested control loops explicit.

Using a set of previously acquired input-output data this simplified model was simulated by passing to the input (\( \delta M \)) a \textbf{P}seudo \textbf{R}andom \textbf{B}inary \textbf{S}equence (PRBS).  The results of this simulation are show in Figure \ref{fig:simres_comp_ol}. The measured and simulated pitch rates (\( q \)) are in agreement even if the simulated system tends to be slower than the actual quadcopter. The pitch angle was not included in the dataset but the simulated pitch angle was included in the results for completeness.

\begin{figure}[htbp]
    \centering
    \input{Figures/Plots/plant_model_sim.tikz}
    \caption[Measured \& simulated open loop responses]{Comparison of the measured and simulated open loop responses of the quadcopter when fed a PRBS input}
    \label{fig:simres_comp_ol}
\end{figure}

\subsection{Mixer Matrix}
\label{sec:simres_mixer}

As discussed in Section \ref{sec:quad_pitch_ctrl}, the pitch rate controller is a PID whose output is the desired pitch moment of the quadcopter. The input of the above model however is the difference in speed between the front and rear propellers. The translation from one to the other is devolved to the mixer matrix.

Recall that the mixer matrix relates the vertical thrust \( T \) and the moments \( L \), \( M \) and \( N \) around, respectively, the pitch, roll and yaw axes to the speeds of all four rotors \(\Omega_1 \), \(\Omega_2 \), \(\Omega_3 \), \( \Omega_4 \) in the following manner 

\begin{equation}
    \begin{bmatrix} \Omega_1^2 \\ \Omega_2^2 \\ \Omega_3^2 \\ \Omega_4^2 \end{bmatrix} = 
        \chi^{-1} \begin{bmatrix} T \\ L \\ M \\ N \end{bmatrix}.
\end{equation}

Considering just the pitch control loop, the only non-null angular momentum term is \( M \). We also know that the real input to the system is the difference in speed between the front and the rear rotor. This allows us to reduce the mixer to a single scalar: 

\begin{equation}
     \chi = 66.67 
\end{equation} 

\subsection{Complete System}

With this last piece of missing information the models obtained so far can be inserted into the pitch control scheme previously presented in Figure \ref{fig:quad_pitch_loop}. The mixer matrix and the plant model combined describe the quadcopter dynamics from the controller output to the pitch rate and the pitch angle is produced by the integrator. The resulting control scheme is shown in Figure \ref{fig:simres_ctrl_scheme}.

\begin{figure}[htbp]
    \centering
    \input{Figures/quad_control_scheme.tikz}
    \caption{Control Scheme used to simulate the pitch dynamics}
    \label{fig:simres_ctrl_scheme}
\end{figure}

The task now is to choose appropriate controller families and reference models. 


\section{Controller Families}
\label{sec:simres_ctrl_fam}

The final choice to be made is the choice of the family of controllers to tune. As discussed in Section \ref{sec:quad_pitch_ctrl} the pitch rate controller is a PID whilst the pitch angle controller is a PD. 

\begin{equation}
    \begin{aligned}
         PD(s) &= K_{p_o} + \frac{K_{d_o}s}{T_fs + 1}  \\
        PID(s) &= K_{p_i} + \frac{K_{i_i}}{s} + \frac{K_{d_i}s}{T_fs + 1}
    \end{aligned}
\end{equation}

VRFT requires that the controller family expressed as a vector of linear transfer functions \( \beta(z) \) such that the parametrised controller is 

\begin{equation}
     C(z;\ \theta) = \beta^T(z)\theta
\end{equation} 

\noindent
where \( \theta \) is the parameter vector. This requirement that the controller family be linear in the parameters means that \( T_f \), the filter time constant of the derivative terms cannot be tuned by the VRFT but must be set beforehand. In the previous \hinf\ controller the filter time constant was manually set to \( T_f = 0.01 \) and this value was left unchanged. 

The controllers written as vectors of linear transfer functions are

\begin{equation}
    \begin{aligned}
        PID(s;\ \theta_i) &= \begin{bmatrix} 1 \quad \frac{1}{s} \quad s \end{bmatrix} \theta_i \\
        PD(s;\ \theta_o) &= \begin{bmatrix} 1 \quad s \end{bmatrix} \theta_o
    \end{aligned} 
\end{equation}

\noindent
where \( \theta_i \) and \( \theta_o \) are, respectively, the parameter vectors for the inner and outer controllers.  

\section{Structure of the Reference Models}
\label{sec:simres_mref_struc}

The choice of the reference model is critical to VRFT, the model should be fast enough to obtain the best possible bandwidth without being unachievable, but there is no need to rely on complex models to obtain satisfying levels of performance. A second order model has been a good starting point. This is the simplest class of models that allows us to tune both its static gain and its bandwidth and it can also be easily extended to account for known properties of the system such as delays.

If we were to choose the reference models without any knowledge of the plant we would be reduced to semi-random guessing. The knowledge of the plant that we have can be used to guide our decisions and hopefully choose a reference model that better represents the actual closed loop transfer function of the plant. 

VRFT also provides for a user-defined weighting function to emphasize performance in certain bands of interest. For simplicity all bands were considered of equal importance and the inner and outer weighting functions were defined as, respectively, \( W_i(z) = 1 \) and \( W_o(z) = 1 \).   

\subsection{Inner Reference Model}
\label{sec:simres_mref_struc_inner}

The reference model for the inner loop derives from a second order model. It was observed algebraically that, considering a \( PID \) controller and a first order plant model \( G \) such that 

\begin{equation}
          PID(s) = K_{p_o} + \frac{K_{i_i}}{s} + \frac{K_{d_i}s}{T_fs + 1}
, \qquad G(s) = \frac{a}{b+s}      
\end{equation}  

\noindent
the resulting closed loop transfer function would be of the form 

\begin{equation}
    F(s) = \mu \frac{1 + s}{s^2 + cs + d}
\end{equation}

\noindent
and, to compensate for this additional zero, it was decided to add a zero to the reference model. This zero was chosen to be in the neighbourhood of the zero naturally emerging from the PID controller for a reasonable range of PIDs. The resulting form of the inner reference model is thus 

\begin{equation}
    M_{R_i}(s) = \frac{\omega_{n_i}^2}{s^2 + 2 \zeta_i \omega_{n_i} s + \omega_{n_i}^2} \frac{s + z_0}{z_0}
\end{equation}

\noindent
where \( \omega_{n_i} \) is the bandwidth of the reference model, \( \zeta_i \) is the damping ratio and \( z_0 \) is the position of the zero.

Initially the specific bandwidth and damping ratio of the reference model were set to achieve similar performance to what had been observed with the pre-existing \hinf controller. During the experimental phase the reference model would be further tuned to extract better performance from the system.

\subsection{Outer Reference Model}
\label{sec:simres_mref_struc_outer}

The structure of the outer reference model was assigned in a similar fashion. It was observed that the closed loop transfer function of the system could be approximated with a second order system without any additional poles or zeroes.  

\begin{equation}
    M_{R_o}(s) = \frac{\omega_{n_o}^2}{s^2 + 2 \zeta_o \omega_{n_o} s + \omega_{n_o}^2}
\end{equation}

\noindent
where \( \omega_{n_i} \) is the bandwidth of the reference model and \( \zeta_i \) is the damping ratio.

\section{Simulation Results}
\label{sec:simres_simres}

Before starting the testing campaign it was decided to validate the applicability of the VRFT approach to the problem of pitch control with a set of simulations. Initially, reference models for the inner and outer loops were developed that mimicked the behaviour of the pre-existing \hinf controllers.  

One such pair on inner and outer reference models is 

\begin{equation}
    \begin{aligned}
        M_{R_i}(s) &= \frac{64s + 320}{s^2 + 72s + 320}\\
        M_{R_o}(s) &= \frac{16}{s^2 + 7.2s + 16}
        \label{eq:simres_refmodels_1}
    \end{aligned}
\end{equation} 

\noindent
where the inner reference model (\( M_{R_i} \)) has a bandwidth of \num{8} \si{\radian\per\second} and a damping ratio of \num{0.9} and the outer reference model (\( M_{R_o} \)) has a bandwidth of \num{4} \si{\radian\per\second} and a damping ratio of \num{0.9}.

The VRFT algorithm, applied with the above reference models produced controllers with the parameters shown in Table \ref{tbl:simres_ctrl_params_norm}.
    
\begin{table}[htpb]
    \centering
    \input{Tables/sim_1_params.tex}

    \caption[VRFT results using an achievable reference model]{Controller parameters synthesised using VRFT with the reference models \eqref{eq:simres_refmodels_1}.}
    \label{tbl:simres_ctrl_params_norm}
\end{table} 

A closed loop simulation of the controllers was performed to ensure that the closed loop performance of the system is sufficiently close to the the reference models. This results of the simulation are shown in Figure \ref{fig:simres_clsim_norm}. The simulated system appears to be slightly slower than the reference model but overall the two are in accordance. 

\begin{figure}[htbp]
    \centering
    \input{Figures/Plots/simres_cl_norm.tikz}
    \caption[Simulated responses of the closed-loop transfer function and the reference model]{Comparison of the responses of the outer reference model \( M_{Ro} \)  and the closed loop transfer function of the system with the controllers of Table \ref{tbl:simres_ctrl_params_norm}. The upper plot shows the complete simulation whilst the lower plot shows a zoomed-in view of the first step.}
    \label{fig:simres_clsim_norm}
\end{figure}

\begin{figure}[htbp]
    \centering
    \input{Figures/Plots/simres_cl_hinf.tikz}
    \caption[Simulated responses of the VRFT \& \hinf tuned controllers with an achievable reference model]{Comparison of the responses of the closed loop transfer function of the system with the VRFT tuned controllers and the pre-existing \hinf controllers with the parameters of Table of Table \ref{tbl:simres_ctrl_params_norm}. The upper plot shows the complete simulation whilst the lower plot shows a zoomed-in view of the first step.}
    \label{fig:simres_clsim_hinf}
\end{figure}

For completeness the Bode plots of both the inner and outer loops and their associated reference models are reported, respectively, in Figures \ref{fig:simres_bodesim_in_norm} and \ref{fig:simres_bodesim_out_norm}.

\begin{figure}[htbp]
    \centering
    \input{Figures/Plots/simres_bode_innerref_norm.tikz}
    \caption[Bode plots for the inner closed-loop transfer function with an achievable reference model]{Comparison of the Bode plots of the inner reference model \( M_{Ri} \)  and the closed loop transfer function for the pitch rate regulation loop with the controllers of Table \ref{tbl:simres_ctrl_params_norm}.} 
    \label{fig:simres_bodesim_in_norm}
\end{figure}

\begin{figure}[htbp]
    \centering
    \input{Figures/Plots/simres_bode_outerref_norm.tikz}
    \caption[Bode plots for the outer closed-loop transfer function with an achievable reference model]{Comparison of the Bode plots of the outer reference model \( M_{Ro} \)  and the closed loop transfer function for the pitch angle regulation loop with the controllers of Table \ref{tbl:simres_ctrl_params_norm}.} 
    \label{fig:simres_bodesim_out_norm}
\end{figure}

As shown in Figure \ref{fig:simres_clsim_hinf} the synthesised controller provides similar behaviour to that of the previously obtained \hinf controller even though the parameters of the controllers differ significantly. 

Having shown, using the simulated model, that performance similar to that of the pre-existing \hinf controller could be achieved with a VRFT tuned controller we looked into improving the performance of the controllers. New inner and outer reference models with bandwidths of, respectively, \num{15} and \num {10} \si{\radian\per\second} were generated

\begin{equation}
    \begin{aligned}
        M_{R_i}(s) &= \frac{225s + 1125}{s^2 + 135s + 1125}\\
        M_{R_o}(s) &= \frac{100}{s^2 + 18s + 100}.
    \end{aligned} 
    \label{eq:simres_refmodels_hi}
\end{equation}

\noindent
Using these reference models for the VRFT yielded the parameters in table \ref{tbl:simres_ctrl_params_hi}. Whilst the controller parameters seem extremely high the simulation shown in Figure \ref{fig:simres_sim_hi} bears out their effectiveness on paper. However, this assumes that the system is accurately described by the model. Since the model of the vehicle does not impose any limits to the closed loop bandwidth this is not the case when the bandwidth of the reference model is high. A test with similar controller parameters run on the actual quadrotor had to be prematurely terminated due to uncontrolled oscillations of the system.

To achieve the requested speed an unrealistic control effort is required. The real world performance is limited by the saturations on the controller outputs and, consequently, the maximal real-world performance was much lower than this.

Clearly, relying on only the simulated model is not sufficient to accurately tune the controllers. An comprehensive set of experiments must be performed on the system to identify the actual upper bound for performance.

\begin{table}[htpb]
    \centering
    \input{Tables/sim_hi_params.tex}

    \caption[VRFT results using an unachievable reference model]{Controller parameters produced by VRFT with the reference models \eqref{eq:simres_refmodels_hi}.}
    \label{tbl:simres_ctrl_params_hi}
\end{table} 

\begin{figure}[htbp]
    \centering
    \input{Figures/Plots/simres_cl_hi.tikz}
    \caption[Simulated responses of the VRFT \& \hinf tuned controllers with an unachievable reference model]{Comparison of the responses of the outer reference model \( M_{Ro} \)  and the closed loop transfer function of the system with the controllers of Table \ref{tbl:simres_ctrl_params_hi}.}
    \label{fig:simres_sim_hi}
\end{figure}

The bode plots of the inner and outer loops with their associated reference models are reported, respectively, in Figures \ref{fig:simres_bodesim_in_hi} and \ref{fig:simres_bodesim_out_hi}. Note the the bode diagrams of the inner loop and its reference model show a significant divergence between the two at higher frequencies. This is an indication that the required performance is difficultly achievable.

\begin{figure}[htbp]
    \centering
    \input{Figures/Plots/simres_bode_innerref_hi.tikz}
    \caption[Bode plots for the inner closed-loop transfer function with an unachievable reference model]{Comparison of the Bode plots of the inner reference model \( M_{Ri} \)  and the closed loop transfer function for the pitch rate regulation loop with the controllers of Table \ref{tbl:simres_ctrl_params_hi}.} 
    \label{fig:simres_bodesim_in_hi}
\end{figure}

\begin{figure}[htbp]
    \centering
    \input{Figures/Plots/simres_bode_outerref_hi.tikz}
    \caption[Bode plots for the outer closed-loop transfer function with an unachievable reference model]{Comparison of the Bode plots of the outer reference model \( M_{Ro} \)  and the closed loop transfer function for the pitch angle regulation loop with the controllers of Table \ref{tbl:simres_ctrl_params_hi}.} 
    \label{fig:simres_bodesim_out_hi}
\end{figure}
