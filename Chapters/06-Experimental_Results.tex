
\chapter{Experimental Results}
\label{chap:expres}

In chapter \ref{chap:simres} the performance of the simulated system was explored and it was showed that, since the model does not account for non-linearities and other complex effects, it is possible (at least theoretically) to achieve unrealistic levels of performance. Tests on the real hardware were conducted in order to find an actual bound to the achievable performance.  

In Section \ref{sec:expres_comps} the actual components of the quadcopter will be detailed. In Section \ref{sec:expres_tuning} the fashion in which the required input-output data was collected will be explained. VRFT has provisions for noise mitigation procedures. Their use will be detailed in Section \ref{sec:expres_noise}. In Section \ref{sec:expres_refmodel} the reference models used for the VRFT will be shown and, finally, in Section \ref{sec:expres_res} the results of the actual experiments on the system will shown and compared to a pre-existing manually tuned \hinf\  controller.

\section{Quadcopter Hardware \& Firmware}
\label{sec:expres_comps}

The quadcopter used for this work was entirely developed at Politecnico di Milano based on the following requirements:

\begin{itemize}
    \item \emph{X} frame of medium size (\num{450} to \num{550} \si{\milli\metre} distance between opposite rotors)
    \item Overall weight of less than \kg{2} 
    \item Payload of at least \kg{0.5}
    \item Flight time of at least 10 minutes
\end{itemize}

The specifications of the components chosen in order to achieve these goals are documented in the following sections. A fixed-pitch actuation scheme was retained since the performance requirements of the system are relatively low. For a more detailed explanation behind these choices see \cite{Giurato2015DesignFixed}

\begin{figure}[htbp]
    \centering

    \begin{subfigure}[t]{0.45\textwidth}
        \centering
        \includegraphics[width=.95\textwidth]{hobby_king_talon_20}
        \caption{Hobby King Talon V2.0 Frame}
        \label{subfig:quad_frame}
    \end{subfigure}
    \begin{subfigure}[t]{0.45\textwidth}
        \centering
        \includegraphics[width=.95\textwidth]{rctimer_hp2814}
        \caption{RCTimer HP2814 Motor}
        \label{subfig:quad_motor}
    \end{subfigure}
    \begin{subfigure}[t]{0.45\textwidth}
        \centering
        \includegraphics[width=.95\textwidth]{rctimer_nfs_esc_30a}
        \caption{RCTimer NFS ESC 30A}
        \label{subfig:quad_esc}
    \end{subfigure}
    \begin{subfigure}[t]{0.45\textwidth}
        \centering
        \includegraphics[width=.95\textwidth]{12_inch_prop}
        \caption{12 inch, \num{4.5} \si{\degree} pitch propellers}
        \label{subfig:quad_prop}
    \end{subfigure}
    \begin{subfigure}[t]{0.45\textwidth}
        \centering
        \includegraphics[width=.95\textwidth]{support}
        \caption{Electronic boards support}
        \label{subfig:quad_support}
    \end{subfigure}
    \begin{subfigure}[t]{0.45\textwidth}
        \centering
        \includegraphics[width=.95\textwidth]{vibr_dampers}
        \caption{Vibration dampers}
        \label{subfig:quad_vibr_damper}
    \end{subfigure}
    
    \caption{Quadrotor Components}
    \label{fig:quad_hk_plat}
\end{figure}

\subsection{Frame}

The frame (Figure \ref{subfig:quad_frame}) is the central building block of the quadcopter. The Talon V2.0 frame, built out of carbon fibre and aluminium was retained due to its low weight and high strength. The distance between opposite motors on this frame is \num{500} \si{\milli\metre}.

\subsection{Motors, ESC \& Propellers}

The motors (Figure \ref{subfig:quad_motor}) are brushless DC motors (BLDC) from the RCTimer High Performance series (HP2814). Since brushless motors are synchronous they cannot be driven by a simple DC current but require an inverter to provide a switching electric signal. This is the \textbf{E}lectronic \textbf{S}peed \textbf{C}ontroller (ESC). In this case, the RCTimer NFS ESC 30A (Figure \ref{subfig:quad_esc}). This combination of motor and driver is able to push the propeller to close to 8000\ \rpm

The propellers mounted on the motors have two \num{12} \si{inch} blades with a \num{4.5}\si{\degree} pitch angle (Figure \ref{subfig:quad_prop}).

\subsection{Battery}

The batteries used on the quadrotor are Turnigy nano-tech \num{4000} \si{\milli\ampere\hour}. These are LiPo 3 cell batteries with a nominal tension of \num{3.7} \si{\volt} per cell for a total tension of \num{11.1} \si{\volt} per battery. The batteries are capable of supplying a constant current of \num{100} \si{\ampere} with peaks of up to \num{200} \si{\ampere} for short periods of time.

\subsection{Vibration Damping}

To reduce the mechanical vibrations transmitted through the frame from the motors a support plate onto which the electronics are mounted was realised (Figure \ref{subfig:quad_support}). This plate is then fixed to the frame with vibration damping rubber feet (Figure \ref{subfig:quad_vibr_damper}).

\subsection{Flight Control Unit \& Firmware}

The flight control unit is the brains of the quadrotor. It was implemented on a \textbf{R}apid \textbf{R}obot \textbf{P}rototyping (R2P) boards~\cite{Bonarini2014R2P}. This is an open source hardware and software framework that enables the rapid development of robotic applications. These boards implement an IMU, serial communication, control of the RC motors with PWM signals and the actual flight control. The modules use a publish/subscribe architecture to communicate.

The control portion is implemented in Simulink and compiled to C++ code. The firmware manages the communication between the sensors and the generated code in order to control the quadcopter. 

\subsection{Test Bed}
\label{sec:expres_test_bed}

All the tests on the quadrotor were performed on a test bed that constrains all translational degrees of freedom as well as the roll and yaw motions. Only the pitch rotation is left unconstrained. This ensures that the tests are repeatable and safe and that an erroneous choice of the controller parameters will not send the system crashing through the room. 

\begin{figure}[htbp]
    \centering

    \begin{subfigure}[t]{0.45\textwidth}
        \centering
        \includegraphics[width=.95\textwidth]{test_bed_1}
        \caption{Test Bed, Front View}
        \label{subfig:quad_test bed_1}
    \end{subfigure}
    \begin{subfigure}[t]{0.45\textwidth}
        \centering
        \includegraphics[width=.95\textwidth]{test_bed_2}
        \caption{Test Bed, Top view}
        \label{subfig:quad_test_bed_2}
    \end{subfigure}
\end{figure}

The test bed is built out of \emph{x-frame} aluminium rods and weighted with sacks of concrete. The upper part of the frame has a smooth rod resting on ball bearings at each extremity for frictionless rotation. The quadrotor is then securely fastened to this rod. 

In the current mounting scheme the rod passes as close as possible to the centre of mass of the system in order to interfere as little as possible with the dynamics of the quadrotor. Because of the physical configuration of the system there is small distance between the rod and the actual centre of mass. In turn, this causes the system to act like a very small pendulum and the test bed adds some damping when the system quadrotor achieves higher pitch angles. In practice this damping is negligible for small oscillations.

The test bed holds the quadcopter high enough that ground effect disturbances are avoided however, since the test takes place in a closed space some recirculation of rotor wakes occurs. This represents a discrepancy when compared to outdoor flight conditions where the rotor wakes develop free from obstacles. Even so, it has been shown in previous work that such a test bed is representative of actual attitude dynamics in flight.

\subsection{Additional Flight Hardware}

The quadcopter also holds a RaspberryPi 2 board used to interface the R2P modules with a ROS network but this functionality was unused for the tests. In addition a small ultrasonic sensor has been mounted on the drone to measure the distance from the ground when landing. 


\section{Tuning Experiment}
\label{sec:expres_tuning}

The most important pre-requisite for data-driven algorithms such as VRFT is the collection of a sufficiently long persistently exciting input-output dataset. 

Since the aim of this work is to tune the controllers on the pitch regulation loop all the tests were performed on the test bed described in Section \ref{sec:expres_test_bed}. The test bed constrains all the degrees of freedom of the vehicle except the pitch rotation. 

The control scheme for the pitch control loop is shown in Figure \ref{fig:expres_vrft_ctrl}. See Section \ref{sec:quad_pitch_ctrl} for a detailed description of each component and the symbols used. 

\begin{figure}[htbp]
    \centering
    \input{Figures/quad_vrft_ctrl.tikz}
    \caption[Pitch control scheme]{Control scheme for VRFT as applied to the pitch control loop where \( q \), the pitch rate of the quadcopter, is analogous to the inner output signal \( y_i \), \( \vartheta \), the pitch angle,  is analogous to the \emph{outer} output signal \( y_o \) and \( \delta \Omega_P \), the change in speed of the propellers, is analogous to the plant input signal \( u \).}
    \label{fig:expres_vrft_ctrl}
\end{figure}

In the cascade VRFT setting the system should be divided into four parts: an \emph{inner} and an  \emph{outer} parametrised controller and and \emph{inner} and an\emph{outer} unknown plant. The controllers are quite obviously the \( PD \) and \( PID \) blocks in Figure \ref{fig:expres_vrft_ctrl}. It follows that the \( \chi G \) blocks representing the dynamics from the requested torque around the pitch axis to the pitch rate is the inner plant and the integrator \( \frac{1}{s} \) computing the pitch angle from the pitch rate is the outer plant.    

The experiments used to gather the input-output data should be performed in \emph{open loop}, providing the torque \( \delta M \) as an input and reading the pitch rate \( q \) and  the pitch angle \( \vartheta \) as the \emph{inner} and \emph{outer} outputs \( y_i \) and \( y_o \). Unfortunately the firmware does not allow us to provide \( \delta M \) as an input directly or read the pitch angle \( \vartheta \) when operating in open loop conditions. Instead we were only able to specify as the input \( \delta \Omega_P \) the difference in the speed of the front and read propellers and read he pitch rate \( q \) as the single output. 

This is not an issue however since, as shown in Section \ref{sec:simres_mixer}, the mixer, considering only the pitch control loop, is a known scalar. Consequently, the \emph{inner} plant input could be computed quite simply from the available data as 

\begin{equation}
    \delta M = \chi^{-1} \delta \Omega_P.
\end{equation}

The pitch angle \(, \vartheta \), the output of the \emph{outer} plant, can also be calculated algebraically. Whilst in the general case the outer plant model is unknown, in our case the it is an integrator. The pitch angle \( \vartheta \) is simply the integral of the pitch rate \( q \) over time. Consequently we were able to calculate the pitch angle as 

\begin{equation}
    \vartheta = \Integrate{q}{t,0,t} 
    \label{eq:tuning_calc_theta}
\end{equation}

With the quadcopter firmware operating in open loop, a \textbf{P}seudo \textbf{R}andom \textbf{B}inary \textbf{S}equence (PRBS) was generated for the input \( \delta \Omega_P \) and fed into the system with a time step of \num{0.01}\si{\second}. This sequence was generated in such way as to reliably excite the dominant dynamics of the system. This input signal and the computed torque around the pitch axis \( \delta M \) are show in Figure \ref{fig:tuning_prbs_inputs}.

\begin{figure}[htbp] 
    \centering
    \input{Figures/Plots/prbs_inputs.tikz}
    \caption[Tuning Experiment Inputs]{Measured \( \delta \Omega_P \) and computed control variable \( \delta M \). Measured signals are represented with a continuous line, computed signals are shown with a dotted line}
    \label{fig:tuning_prbs_inputs}
\end{figure}

The output measurement, the pitch rate signal \( q \),  was acquired and logged by the quadcopter itself using its on-board IMU with a time step of \num{0.01}\si{\second}. The pitch angle \( \vartheta \) was computed from the pitch rate measurements as shown in equation \eqref{eq:tuning_calc_theta}. The measured pitch rate and the computed pitch angle are shown in Figure \ref{fig:tuning_prbs_outputs}.

\begin{figure}[htbp] 
    \centering
    \input{Figures/Plots/prbs_outputs.tikz}
    \caption[Tuning Experiment Outputs] {Measured pitch rate (\( q \)) and computed pitch angle (\( \vartheta \)). Measured signals are represented with a continuous line, computed signals are shown with a dotted line}
    \label{fig:tuning_prbs_outputs}
\end{figure}


\section{Noise Considerations}
\label{sec:expres_noise}

Like all real world signals the input-output signals measured on the quadcopter are noisy. As detailed in Section \ref{sec:vrft_noisy_data}, VRFT solves the problem of reducing the noise induced bias in the criterion it minimises with an instrumental variable approach. This requires either the collection of a second input-output dataset or the identification of a high-order model of the plant that can be used to generate such a dataset. 

The toolbox used in this work to perform the VRFT \cite{CampiVRFT} uses the second method. It accepts as an input the order of an ARX model that will be used to generate the second dataset. 

To identify the order of the ARX model of the plant to be used the measured input data was loaded into the \verb|MATLAB| System Identification tool and de-biased. Using the polynomial estimation function It was observed that the model that provided the best fit was an \( ARX(17, 7) \).


\section{Reference Models}
\label{sec:expres_refmodel}

The structure of the reference models was previously defined in Section \ref{sec:simres_mref_struc} based on the knowledge provided by the available models. What is left is to identify an \emph{inner} and an \emph{outer} reference model that maximises the achievable performance. 

This was done with a series of tests. VRFT makes this incredibly fast; a single input-output dataset can be used to tune any number of controllers. Once programmed onto the quadcopter the system was put through a series of step inputs and any controllers that unexpectedly yielded unstable control loops were immediately discarded to avoid what Elon Musk famously referred to as \emph{rapid unscheduled disassembly} events. The final choice of parameters represents the best trade-off between the rise time and the settling time. 

This also illustrates one of the limitations of VRFT: it does not provide any kind of guarantee that the closed loop transfer function will be stable. 

\subsection{Inner Reference Model}

The structure of the \emph{inner} reference model decided in Section \ref{sec:simres_mref_struc_inner} is 

\begin{equation*}
    M_{R_i}(s) = \frac{\omega_{n_i}^2}{s^2 + 2 \zeta_i \omega_{n_i} s + \omega_{n_i}^2} \frac{s + z_0}{z_0}
\end{equation*}

\noindent
where \( \omega_{n_i} \) is the bandwidth of the reference model, \( \zeta_i \) is the damping ratio and \( z_0 \) is the position of the zero.

At the conclusion of the testing campaign It was observed that the best tradeoff between the rise time and the settling time was obtained with a bandwidth \( \omega_{n_i} = 8\si{\radian\per\second} \), a damping-ratio \( \zeta_i = 0.9 \) and a zero placed in \( z_0 = 5 \)

\begin{equation}
    M_{R_i}(s) = \frac{64s + 320}{5s^2 + 72s + 320} \frac{s - 5}{5}
\end{equation}

The VRFT procedure is defined only for discrete time systems but the quadcopter firmware considers only continuous time models. This mismatch was overcome by discretising the models prior to performing the VRFT and converting the output back to a continuous time model. 

The discretised form of the inner reference model, considering a time step \( T_s = 0.01 \)\si{\second} is

\begin{equation}
     M_{R_i}(z) = \frac{0.1221z - 0.1162}{z^2 - 1.86z +.8659}.
     \label{eq:expres_inner_refmodel}
 \end{equation} 

\subsection{Outer Reference Model}

In Section \ref{sec:simres_mref_struc_inner} it was decided that the best form for the \emph{outer} reference model is a simple second order model. At the conclusion of the testing campaign the model providing the best traedoff between the rise time and the settling time had a bandwidth of \( \omega_{n_o} = 4\si{\radian\per\second} \) and a damping ration of \( \zeta_o = 0.9 \)

\begin{equation}
    M_{R_o}(s) = \frac{16}{s^2 + 7.2s + 16}
\end{equation}

\noindent
and the discretised form of this reference model is

\begin{equation}
    M_{R_o}(z) = \frac{7.81z + 7.625}{z^2 - 1.929z + 0.9305}10^{-4}
    \label{eq:expres_outer_refmodel}
\end{equation}


\section{Results \& Comparison}
\label{sec:expres_res}

Running the VRFT procedure on both the \emph{inner} and \emph{outer} loops with the input-output data shown in Figures \ref{fig:tuning_prbs_inputs} and \ref{fig:tuning_prbs_outputs}, the reference models \eqref{eq:expres_inner_refmodel} and \eqref{eq:expres_outer_refmodel} and considering an \( ARX(17, 7) \) model for the noise mitigation leads to the controller parameters shown in table \ref{tbl:expres_params}. Also shown are the parameters for the pre-existing \hinf controller to be used as a point of reference. 

\begin{table}[htpb]
    \centering
    \input{Tables/final_params.tex}

    \caption[VRFT \& \hinf controller parameters]{Controller parameters for both the VRFT and \hinf tuned controllers. Note that \( T_f \), the filter time constant, was set manually for both controllers and is not a direct product of the tuning procedures.}
    \label{tbl:expres_params}
\end{table} 

\subsection{Set-Point Tracking}

To validate the controllers a test sequence with steps of increasing amplitude was generated an fed as a set a set-point to the quadcopter. For safety the tests were performed with the quadcopter securely fastened to the test-bed. The result of one such run with the VRFF tuned controllers of Table \ref{tbl:expres_params} are shown in Figure \ref{fig:expres_vrft_sp}.

\begin{figure}[htbp]
    \centering
    \input{Figures/Plots/pitch_set_point_tracking_vrft.tikz}

    \caption[VRFT set-point tracking]{Pitch angle set-point tracking using the controller synthesised with VRFT and the reference models \eqref{eq:expres_inner_refmodel} and \eqref{eq:expres_outer_refmodel}}
    \label{fig:expres_vrft_sp}
\end{figure}

 The test was repeated 10 times using . 
the both the VRFT and \( \hinf \) tuned controllers and the average mean square error was computed. 

The set-point tracking performance of the quadcopter with, respectively, the VRFT and \( \hinf \)  controllers during one, randomly chosen, run  is shown in Figures \ref{fig:expres_vrft_sp} and \ref{fig:expres_hinf_sp}.  


\begin{figure}[htbp]
    \centering
    \input{Figures/Plots/pitch_set_point_tracking_hinf.tikz}

    \caption[\hinf set-point tracking]{Pitch angle set point tracking using the pre-existing \( \hinf \) controller}
    \label{fig:expres_hinf_sp}
\end{figure}

The VRFT and \hinf tuned  controllers were very similar in the simulation runs with the VRFT controller being marginally slower than the \hinf controller. In these experiments this trend is inverted. The VRFT tuned controller is marginally faster than the \hinf controller. This can be explained quite easily: the \hinf tuned controller is based on an identified model of the system and can only ever be as good as the identified model. The VRFT tuned controller has the advantage of being tuned on real data collected on the system and is thus exempt from modelling errors.  

The mean square error, comparing the measured pitch angle to the set-point, was computed for each test and averaged. The values for the VRFT and \hinf tuned controllers are shown in the first column of table \ref{tbl:expres_mse}. The mean square error of the VRFT controller, whilst a little higher than that of the \hinf controller is still within acceptable bounds. It can be explained by the slightly more oscillatory nature of the VRFT tuned controller. 

\subsection{Disturbance Rejection}

The disturbance rejection properties of the firmware were considered using a similar method. The firmware of the quadcopter provides a method to introduce a step disturbance on the speed of the motors. This was used to repeatedly reduce the lift generated by the motors on the front of the quadcopter (motors 1 \& 2) by 10 \%. The test was repeated 10 times with both the VRFT and \hinf tuned controllers and the average of the mean square error was computed.

The disturbance rejection performance of the quadcopter with, respectively, the VRFT and \( \hinf \)  controllers during one, randomly chosen, run are shown in Figures \ref{fig:expres_vrft_disturb} and \ref{fig:expres_hinf_disturb}.  

\begin{figure}[htbp]  
    \centering
    \input{Figures/Plots/disturbed_control_vrft.tikz}

    \caption[VRFT disturbance rejection]{Disturbance rejection properties of the controller synthesised with VRFT and the reference models \eqref{eq:expres_inner_refmodel} and \eqref{eq:expres_outer_refmodel}.The pitch angle set point is fixed at \num{0}\si{\degree} and a disturbance is applied. The disturbance is a 10\si{\percent} drop in the speed of the front rotors (rotors 1 \& 2).}
    \label{fig:expres_vrft_disturb}
\end{figure}

\begin{figure}[htbp]  
    \centering
    \input{Figures/Plots/disturbed_control_hinf.tikz}

    \caption[\hinf disturbance rejection]{Disturbance rejection properties of the pre-existing \hinf Tuned controller. The pitch angle set point is fixed at \num{0}\si{\degree} and a disturbance is applied. The disturbance is a 10\si{\percent} drop in the speed of the front rotors (rotors 1 \& 2).}
    \label{fig:expres_hinf_disturb}
\end{figure}

It is immediately apparent that the VRFT tuned controller offers significant improvements to the steady state error. The \hinf tuned controller settles with a steady state error of several degrees whereas the VRFT tuned controller achieves zero state error. In addition the control effort required by both controllers is quite similar even if the VRFT tuned controller has slightly higher peaks. 

The improvement is reflected in the steady state error of the two controllers as shown in Table \ref{tbl:expres_mse}. The mean square error of the VRFT tuned controller is slightly lower than that of the \hinf controller. 

\begin{table}[htbp]
    \centering
    \input{Tables/mse.tex}

    \caption[Mean MSE of VRFT \& \hinf experiments]{Mean of MSE for validation experiments considering both VRFT and \( \hinf \) tuned controllers}
    \label{tbl:expres_mse}
\end{table} 

