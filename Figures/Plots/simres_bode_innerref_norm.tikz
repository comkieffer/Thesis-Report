
\tikzsetnextfilename{simres_bode_innerref_norm}

\def\axisdefaultwidth{12cm}
\def\axisdefaultheight{5cm}

\begin{tikzpicture}
    \begin{axis}[
          name=top_plt 
        , height = 4.5cm, width=12cm,
        , xmode = log
        , xticklabels = {,,}        
        , ylabel = Mag. (dB)
        , yticklabel style={
            /pgf/number format/fixed,
            /pgf/number format/precision=0
        }           
        , xmin = 1, xmax = 1000
        , no markers
        , xminorgrids, ymajorgrids
        ]
        \addplot[dashdotted, blue] table[x=omega,y=mag] {Figures/Plots/simres_innerref_bode_norm.dat};
        \addplot                   table[x=omega,y=mag] {Figures/Plots/simres_innervrft_bode_norm.dat};
    \end{axis}

    \begin{axis}[
          at=(top_plt.below south west), anchor=above north west
        , height = 3.5cm, width = 12cm,
        , xmode = log
        , xlabel = $\omega (\si{\radian})$
        , ytick = {-90, 0}
        , ylabel = Phase (\si{\degree})
        , yticklabel style={
            /pgf/number format/fixed,
            /pgf/number format/precision=0
        }           
        , xmin = 1, xmax = 1000
        , legend style = { 
              at={(0.5, -1.5cm) }, anchor = north
            , /tikz/every even column/.append style={column sep=0.5cm}
        } 
        , legend columns = {2}
        , no markers
        , xminorgrids, ymajorgrids
        ]
        \addplot[dashdotted, blue] table[x=omega,y=phase] {Figures/Plots/simres_innerref_bode_norm.dat};
        \addplot                   table[x=omega,y=phase] {Figures/Plots/simres_innervrft_bode_norm.dat};

        \legend{Reference Model, Closed Loop}
    \end{axis}

\end{tikzpicture}
