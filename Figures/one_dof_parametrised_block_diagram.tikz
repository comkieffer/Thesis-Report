
\tikzsetnextfilename{one_dof_parametrised_bd}

\begin{tikzpicture}[auto, >=latex]
    % Draw the main control loop nodes
    \draw
    node[input                           ] (input)        { }
    node[sum    , right of = input       ] (errorsum)     { }
    node[block  , right of = errorsum  , node distance = 2.0cm] (controller)   {$ C(z;\ \rho)$}
    node[block  , right of = controller, node distance = 2.5cm] (plant)        {$ G(z) $}
    node[sum    , right of = plant     , node distance = 2.0cm] (disturbfork)  { }
    node[hidden , right of = disturbfork ] (outputfork)   { }
    node[output , right of = outputfork  ] (output)       { };
    
    \draw
    node[hidden , below of = plant       ] (retroaction)  { $ C_y $};
    
    % Add the noise signal
    \draw
    node[hidden, above of = disturbfork] (disturbin) {};

    % Connect everything together
    \draw[->] (input) node[above] {$ r $} -- (errorsum);
    \draw[->] (errorsum) -- (controller) node[midway] {$ e $};
    \draw[->] (controller) -- (plant) node[midway] {$ u $};
    \draw[->] (plant) -- (disturbfork);
    \draw[->] (disturbfork) -- (outputfork) -- (output) node[above] {$y$};
    
    \draw[->] (outputfork) |- (retroaction) -| node[pos=0.9] {$-$} (errorsum);

    \draw[->] (disturbin) node[right] {$ \nu $} -| (disturbfork);

\end{tikzpicture}


