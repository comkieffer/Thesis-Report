
\tikzsetnextfilename{quad_vrft_ctrl}

\usetikzlibrary{patterns}
\tikzstyle{block} = [draw, fill=white, rectangle, minimum height=3em, minimum width=3em, node distance = 1.5cm]


\begin{tikzpicture}[auto, trim left, scale=0.075, >=latex]
    % Draw the main control loop nodes
    \draw
    node[input] (input) {}
    node[sum   , right of = input        , node distance = 1.50cm] (osum)        {}
    node[block , right of = osum         , node distance = 1.50cm] (ocontroller) {$ PD $}
    node[sum   , right of = ocontroller  , node distance = 1.25cm] (isum)        {}
    node[block , right of = isum         , node distance = 1.50cm] (icontroller) {$ PID $}
    node[block , right of = icontroller  , node distance = 2.50cm] (iplant)       {$ \chi G $}
    node[hidden, right of = iplant       , node distance = 1.25cm] (ioutputfork) {}
    node[block , right of = ioutputfork  , node distance = 1.50cm] (oplant)      {$ \frac{1}{s} $}
    node[hidden, right of = oplant       , node distance = 1.25cm] (ooutputfork) {}
    node[output, right of = ooutputfork  , node distance = 0.75cm] (output)      {}

    node[hidden, below of = iplant                               ] (iretroaction) {}
    node[hidden, below of = iretroaction , node distance = 0.25cm] (oretroaction) {};

    % Draw the `Inner Plant' & `Outer Plant' labels.
    % The `yshift' coordinates are waay off but somehow work. No idea how.
    \draw[
          decorate, decoration = {brace, amplitude = 5pt}
    ] ([yshift = 9.6cm] iplant.west) -- ([yshift = 9.6cm] iplant.east) node[black, midway, yshift = 0.5em] { \small Inner Plant };

    \draw[
          decorate, decoration = {brace, amplitude = 5pt}
    ] ([yshift = 9.6cm] oplant.west) -- ([yshift = 9.6cm] oplant.east) node[black, midway, yshift = 0.5em] { \small Outer Plant };


    % Connect everything together
    \draw[->]       (input) -- (osum)        node[midway] {$ \vartheta_0 $};
    \draw[->]        (osum) -- (ocontroller) node[midway] { };
    \draw[->] (ocontroller) -- (isum)        node[midway] { };
    \draw[->]        (isum) -- (icontroller) node[midway] { };
    \draw[->] (icontroller) -- (iplant)      node[midway] {$ \delta M $};
    \draw[->]      (iplant) -- (ioutputfork) node[above]  {$ q $}           -- (oplant) ;
    \draw[->]      (oplant) -- (ooutputfork)                                -- (output) node[midway] {$ \vartheta $};
    
    % Draw retroactions
    \draw[->] (ioutputfork) |- (iretroaction) -| (isum) node[pos=0.9] {$-$};
    \draw[->] (ooutputfork) |- (oretroaction) -| (osum) node[pos=0.9] {$-$};
    
\end{tikzpicture}

    
