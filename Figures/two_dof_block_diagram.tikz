
\tikzsetnextfilename{two_dof_bd}


\begin{tikzpicture}[auto, >=latex]
    % Draw the main control loop nodes
    \draw
    node[input                           ] (input)       { }
    node[block  , right of = input       ] (compensator) { $ C_r $}
    node[sum    , right of = compensator ] (errorsum)         { }
    node[block  , right of = errorsum    ] (plant)       { $ G_0 $}
    node[sum    , right of = plant       ] (disturbfork) { }
    node[hidden , right of = disturbfork ] (outputfork)  { }
    node[output , right of = outputfork  ] (output)       { };
    
    \draw
    node[block  , below of = plant       ] (controller)  { $ C_y $};
    
    % Add the noise signal
    \draw
    node[hidden, above of = disturbfork] (disturbin) {};

    % Connect everything together
    \draw[->] (input) node[above] {$ r $} -- (compensator);
    \draw[->] (compensator) -- (errorsum);
    \draw[->] (errorsum) -- (plant) node[midway] {$u$};
    \draw[->] (plant) -- (disturbfork);
    \draw[->] (disturbfork) -- (outputfork) -- (output) node[above] {$y$};
    
    \draw[->] (outputfork) |- (controller);
    \draw[->] (controller) -| node[pos=0.9] {$-$} (errorsum);

    \draw[->] (disturbin) node[right] {$ \nu $} -| (disturbfork);

\end{tikzpicture}


