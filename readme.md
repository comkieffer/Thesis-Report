
# Thesis-Report 

This is the final report for my master's thesis: "Data-Driven Attitude Control Design For Multirotor UAVs". An up-to-date, compiled, version of this report can always be found [here](https://comkieffer.gitlab.io/Thesis-Report/thesis.pdf) thanks to `gitlab-ci` and the associated latex sources are in [this](https://gitlab.com/comkieffer/Thesis-Report) repo. 

You might also be interested in the presentation I used when defending my thesis. The repo for the presentation can be found [here](https://gitlab.com/comkieffer/Thesis-Presentation) and an up-to-date compiled version can be found [here](https://comkieffer.gitlab.io/Thesis-Presentation/presentation.pdf). 

The code for my thesis is located [here](https://gitlab.com/comkieffer/Thesis-Code). It is comprised of `MATLAB` code to design and tune the controllers. 
